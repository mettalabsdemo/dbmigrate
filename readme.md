# pg2mysql Readme

### Purpose:
The purpose of the pg2mysql script is to facilitate the copying of data from
Greenplum to MySQL and find out differences between a table in Greenplum and MySQL.
The script has three modes: copy, diff, and resume. Each of these modes can be
activated by passing the appropriate command line parameters. 

####Copy Mode
Copy mode is activated by calling the script with no parameters or with the -t
parameter. In copy mode, the
scripts copies over all the data from Greenplum to MySQL.The command is
`python pg2mysql.py [-t TABLE]`.

####Diff Mode
Diff mode is activated by calling the script with --diff True. Diff mode also
expects a table name (-t parameter) and target database (-d parameter).
Optionally, a list of columns to be ignored during the diff can be added in the
end. In diff mode, the scripts checks if the schema of the table being imported from
Greenplum is same as the schema of the table in MySQL. If they are, the script
proceeds to run the diff stored procedure. If they are not, the script produces
a list of the columns that do not match, an empty matching YAML file template to
create mappings between mismatched columns, and exits. The command for diff mode
is
`python pg2mysql.py --diff True -t TABLE -db TARGET_DB COMMA SEPARATED LIST OF COLUMNS`

####Resume Mode
Resume mode is activated by calling the script with a --app option that
takes a mapping file. In this mode, the script loads the configurations from the
mapping file, applies the schema mappings, creates a DB with the schema in
MySQL, and copies the data. The Resume mode completes by running the diff stored
procedure and produces a report. The command to run in resume mode is:
`python pg2mysql.py --resume True --app app_name` 

## Check the current status section to see which features are implemented.

## Installation
## Eventually there will be a setup.py that will fetch all dependencies.
For now the dependencies must manually be installed.The dependencies are:

1. mysql = "MySQL-python==1.2.5"
2. psycopg2 = "psycopg2==2.6.1"
3. docopt = "docopt==0.6.2"
4. futures = "futures==3.0.4"
5. pyyaml = "PyYAML==3.11"
6. memory_profiler = "memory-profiler==0.40"
7. fabric = "Fabric==1.10.2"

## Current status:
```
- [X] Support copy mode of the script
- [ ] Support diff mode of the script
- [ ] Support resume mode of the script
- [ ] Create unit tests for Fabric support and implment Fabric support for remote read and write
```
