import sys
import datetime
import subprocess
import re
from concurrent.futures import ThreadPoolExecutor, as_completed, wait, ALL_COMPLETED
from perf.timer import Timer
# db driver
import psycopg2
from psycopg2.extras import DictCursor
# application imports
from db_constructs import Database, Table, Column
from shell_utils import shell_util
from applogger.logger import Logger
import queries
from ddl_writers import mysql_writer


class PGSQLDB(Database):

    SEARCH_PATH = "SET search_path TO {schema}"
    COPY_COMMAND_TEMPLATE = "COPY (SELECT {fields} FROM {table}) TO STDOUT"

    def __init__(self, **kwargs):
        kwargs["db_config"]["sql_flavor"] = "pgsql"
        self.logger = Logger("PGSQL_LogHandler",
                             kwargs["logroot"]+"/log")
        super(PGSQLDB, self).__init__(db_config=kwargs["db_config"],
                                      app_name=kwargs["app_name"],
                                      app_config=kwargs["app_config"],
                                      credentials=kwargs["credentials"],
                                      target=kwargs["target"],
                                      logger=self.logger)
        self.schema = kwargs["db_config"]["schema"]
        self.pg_dump_status = False
        self.export_only = kwargs["export_only"]
        self.export_schema = kwargs["export_schema"]
        self.logger.debug("SCHEMA USED FOR EXPORT")
        self.logger.debug(self.export_schema)
        conn = self.return_cursor()[0]
        if conn:
            conn.close()
        else:
            # log error connecting to DB
            pass

    def return_cursor(self):
        conn_string = "host={host} dbname={db} user={user}\
        password={pwd}".format(host=self.host, db=self.db,
                               user=self.user, pwd=self.pwd)
        try:
            conn = psycopg2.connect(conn_string)
            cursor = conn.cursor()
            return conn, cursor
        except psycopg2.Error:
            return None

    # creates the pgsql schema dump and returns True if successful
    def export_pgsql_schema(self):
        conn, cursor = self.return_cursor()
        cursor.close()
        cursor = conn.cursor(cursor_factory=DictCursor)
        conn.autocommit = True
        table_query = queries.TABLE_QUERY.format(
            schema=self.schema)
        cursor.execute(table_query)
        tables = cursor.fetchall()
        db_tables = []
        for table in tables:
            db_tables.append(table[0])
        tables = []
        for db_table in db_tables:
            # pkey (str) - The primary keys in this table
            pkey_query = queries.PRIMARY_KEY.format(
                table=self.schema + "." + db_table)
            cursor.execute(pkey_query)
            # pkeys (list) - pkey field(s)
            pkey_list = cursor.fetchall()
            # pkey_cols (list) - All columns in the pkey
            pkey_cols = [pkey["pkey"] for pkey in pkey_list]
            col_query = queries.COLUMN_QUERY.format(schema=self.schema,
                                                    table=db_table)
            cursor.execute(col_query)
            columns = cursor.fetchall()
            current_columns = []
            for column in columns:
                current_columns.append(
                    Column(
                         column_name=column["column_name"],
                         data_type=column["data_type"],
                         character_maximum_length=
                         column["character_maximum_length"],
                         is_nullable=False
                         if column["is_nullable"] == "NO" else True,
                         is_pkey=True if column["column_name"] in pkey_cols
                         else False,
                         column_default=column["column_default"],
                         ordinal_position=column["ordinal_position"]
                     )
                )
            # print "------"*20
            current_table = Table(db_table, current_columns)
            tables.append(current_table)
        self.tables = tables
        cursor.close()
        conn.close()

    def convert_psql_mysql(self):
        try:
            for db_table in self.tables:
                # print db_table.table_name
                cols = db_table.get_columns()
                for col in cols:
                    # print col
                    mysql_writer.convert_to_mysql(col)
            if self.export_schema is not None:
                return self.export_schema
            else:
                self.serialize_to_ddl(self.app_name)
                return self.write_output_file()
        except Exception, e:
            self.logger.critical("Error converting PGSQL schema to MySQL")
            print e
            # log error writing to mysql
            pass

    def serialize_to_ddl(self, export_db_name):
        db_create_string = "CREATE DATABASE IF NOT EXISTS \
                             {db_name};".format(
                            db_name=self.app_name)
        db_use_string = "USE {db_name};".format(db_name=export_db_name)
        self.DDL_statements = [db_create_string, db_use_string]
        for table in self.tables:
            # self.logger.debug(mysql_writer.serialize_to_mysql_ddl(table))
            self.DDL_statements.append(mysql_writer.serialize_to_mysql_ddl(table))

    def write_output_file(self):
        try:
            target_schema_file = self.app_name + "_export" + ".sql"
            target_schema_file = self.app_root + "/" + target_schema_file
            with open(target_schema_file, "w") as sql_file:
                for DDL_statement in self.DDL_statements:
                    sql_file.write(DDL_statement + "\n")
            return target_schema_file
        except IOError:
            print "Error writing target schema file "
            print target_schema_file
            print sys.exc_info()
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    def copy_to_csv_init(self, callback):
        csv_copy_futures = {}
        with ThreadPoolExecutor(max_workers=5) as executor:
            tables_to_copy = [self.get_table(t) for t in self.export_tables]\
                if self.export_tables else self.tables
            for table in tables_to_copy:
                # construct the copy query. The copy query is of the form
                # SELECT col_i, col_j::type FROM table_name
                # where the cast_mapping attribute of col_j is not None
                copy_query_col_names = []
                for column in table.get_columns():
                    self.logger.debug(column)
                    copy_query_col_names.append(column.column_name
                                                if not column.is_cast
                                                else "{0}::{1}".format(
                                                    column.column_name,
                                                    column.cast_type))
                copy_query = ", ".join(copy_query_col_names)
                self.logger.debug("Initializing thread for {0} "
                                  "with query {1}".format(
                                        table.table_name,
                                        copy_query))
                self.logger.debug(self.COPY_COMMAND_TEMPLATE.format(
                            fields=copy_query,
                            table=table.table_name))
                csv_copy_futures[executor.submit(self.copy_to_csv,
                                                 table.table_name,
                                                 copy_query)] =\
                    table.table_name
            if self.export_only:
                for csv_copy_future in csv_copy_futures:
                    csv_copy_future.__setattr__("name", csv_copy_futures[csv_copy_future])
                    csv_copy_future.add_done_callback(callback)
            for future in as_completed(csv_copy_futures):
                self.logger.info("Done creating TSV for {0}".format(future.name))
            wait(csv_copy_futures, timeout=None, return_when=ALL_COMPLETED)
            self.pg_dump_status = True

            # prov_dir_mputcha_SCH_prov_export_murs_201604152146

    def copy_to_csv(self, table_name, copy_query):
        """

        Args:
            table_name:
            copy_query:

        Returns:

        """
        try:
            conn, cursor = self.return_cursor()
            conn.autocommit = True
            cursor.execute('SET search_path TO ' + self.schema)
            cursor.execute('SET role TO {user}'.format(user=self.user))
            self.logger.debug("Role set to " + self.user)
            with Timer() as copy_timer:
                copy_command = self.COPY_COMMAND_TEMPLATE.format(
                            fields=copy_query,
                            table=table_name)
                self.logger.debug("COPY COMMAND " + copy_command)
                csv_export_file_name = self.app_root + "/{table}.tsv".format\
                    (table=table_name)
                with open(csv_export_file_name, "w") as csv_export:
                    cursor.copy_expert(copy_command, csv_export)
            print "COPIED {table} in {time} seconds".format(table=table_name, time=copy_timer.secs)
            cursor.close()
            conn.close()
        except:
            print "Error copying {table}".format(table=table_name)
            print(sys.exc_info())
            raise
