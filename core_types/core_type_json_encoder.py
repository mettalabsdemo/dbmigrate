import json
from db_constructs import Table

class CoreTypeJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance((obj), Table):
            return obj.table_name
        return json.JSONEncoder.default(self, obj)
