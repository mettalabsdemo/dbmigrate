COLUMN_QUERY = "SELECT column_name, data_type, ordinal_position, " \
               "column_default, is_nullable, character_maximum_length " \
               "FROM information_schema.columns WHERE " \
               "table_schema = '{schema}' AND table_name   = '{table}';"

PRIMARY_KEY = "SELECT a.attname as pkey, " \
              "format_type(a.atttypid, a.atttypmod) AS data_type " \
              "FROM   pg_index i JOIN   " \
              "pg_attribute a ON a.attrelid = i. indrelid " \
              "AND a.attnum = ANY(i.indkey) " \
              "WHERE  i.indrelid = '{table}'::regclass AND i.indisprimary;"

TABLE_QUERY = "SELECT table_name FROM information_schema.tables " \
                  "WHERE table_schema = '{schema}';"

MYSQL_PKEY_QUERY = 'SELECT * FROM information_schema.table_constraints t LEFT JOIN information_schema.key_column_usage k ' \
                   'USING(constraint_name,table_schema,table_name) WHERE ' \
                   "t.constraint_type='PRIMARY KEY'" \
                    'AND t.table_schema=DATABASE() AND    t.table_name = "providers_practices"'


# -t strenuus_chn_raw_20160427t1958.provider_network_hashes -t strenuus_chn_raw_20160427t1958.normalized_provider_phone -t strenuus_chn_raw_20160427t1958.provideraddress -t strenuus_chn_raw_20160427t1958.providernetwork -t strenuus_chn_raw_20160427t1958.provider_network_exception