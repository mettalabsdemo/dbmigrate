import sys
sys.path.append("/work/code/prov_dir/util/dbcopy/pg2mysql")

import psycopg2
from psycopg2 import OperationalError
from psycopg2.extras import DictCursor
from applogger.logger import Logger
import queries
from ddl_writers import mysql_writer

LOGGER = Logger("core_types.table.main","./app.log")

class Table(object):
    def __init__(self, table_name, columns=[]):
        """
        Returns a new instance of a table with given name and columns
        Args:
            table_name (str): Name of the table
            columns (list(Columns)): List of columns in this table

        Returns:
            (Table)
        """
        self.table_name = table_name
        self.columns = columns
        # self.logger = Logger("core_types.Table_logger", "./app.log")

    def add_column(self, column):
        """
        Adds a column to the given table
        Args:
            column (Column): The column to be added

        Returns:
            None
        """
        self.columns.append(column)

    def column(self, column_name):
        """
        Returns a column with a given name
        Args:
            column_name (str): Name of the column to be returned

        Returns:
            (Column) - Column with given name
        """
        return None

    def get_columns(self,):
        """

        Returns:
            (list(Columns)) - Returns all columns in this table sorted by the
            ordinal position of the column
        """
        return sorted(self.columns,
                      key=lambda column: column.ordinal_position)


class Column(object):
    def __init__(self, **kwargs):
        """
        Returns a new column with attributes defined in kwargs
        Args:
            **kwargs (keyword args): Args with column_name, datatype,
             length, ordinal_position, nullable, is_pkey, default

        Returns:
            (Column)
        """
        # self.logger = Logger("core_types.Column", "./app.log")
        try:
            self.column_name = kwargs["column_name"]
            self.data_type = kwargs["data_type"]
            self.character_maximum_length = kwargs["character_maximum_length"]
            self.ordinal_position = kwargs["ordinal_position"]
            self.is_nullable = kwargs["is_nullable"]
            self.is_pkey = kwargs["is_pkey"]
            self.column_default = kwargs["column_default"]
            self.auto_increment = ""
            self.mysql_type = None
        except KeyError, k:
            # self.logger.critical("{0} missing in column creation. Exiting".
            #                      format(k))
            print k

    def set_pkey(self, is_pkey):
        self.is_pkey = is_pkey

    def __repr__(self):
        return "Column Name: {column_name}; Datatype: {data_type}; " \
               "Character Length: {character_maximum_length}; " \
               "Is Nullable: {is_nullable}; Is PKey: {is_pkey}; " \
               "Default value: {column_default}; " \
               "Ordinal position: {ordinal_position}".format(
                column_name=self.column_name, data_type=self.data_type,
                character_maximum_length=self.character_maximum_length,
                is_nullable=self.is_nullable, is_pkey=self.is_pkey,
                column_default=self.column_default,
                ordinal_position=self.ordinal_position
                )


def main():
    schema = "prov_export_murs"
    try:
        conn = psycopg2.connect(host="den-gp2-master01",
                            dbname="prov_dir_mputcha",
                            user="kgomadam",
                            password="dbp455")
    except OperationalError, o:
        # LOGGER.critical("Error connecting {0}".format(o))
        print o
    else:
        try:
            cursor = conn.cursor(cursor_factory=DictCursor)
        except OperationalError, o:
            # LOGGER.critical("Error getting cursor {0}".format(o))
            print o
        else:
            table_query = queries.TABLE_QUERY.format(
                schema=schema)
            cursor.execute(table_query)
            tables = cursor.fetchall()
            db_tables = []
            for table in tables:
                db_tables.append(table[0])
        tables = []
        for db_table in db_tables:
            # pkey (str) - The primary keys in this table
            pkey_query = queries.PRIMARY_KEY.format(
                table=schema + "." + db_table)
            cursor.execute(pkey_query)
            # pkeys (list) - pkey field(s)
            pkey_list = cursor.fetchall()
            # pkey_cols (list) - All columns in the pkey
            pkey_cols = [pkey["pkey"] for pkey in pkey_list]
            col_query = queries.COLUMN_QUERY.format(schema=schema,
                                                    table=db_table)
            cursor.execute(col_query)
            columns = cursor.fetchall()
            current_columns = []
            for column in columns:
                current_columns.append(
                    Column(
                         column_name=column["column_name"],
                         data_type=column["data_type"],
                         character_maximum_length=
                         column["character_maximum_length"],
                         is_nullable=False
                         if column["is_nullable"] == "NO" else True,
                         is_pkey=True if column["column_name"] in pkey_cols
                         else False,
                         column_default=column["column_default"],
                         ordinal_position=column["ordinal_position"]
                     )
                )
            # print "------"*20
            current_table = Table(db_table, current_columns)
            tables.append(current_table)
        cursor.close()
        conn.close()
        for db_table in tables:
            # print db_table.table_name
            cols = db_table.get_columns()
            for col in cols:
                # print col
                mysql_writer.convert_to_mysql(col)
                # if col.column_default:
                #     print col
            # LOGGER.debug(mysql_writer.serialize_to_mysql_ddl(db_table))
            print mysql_writer.serialize_to_mysql_ddl(db_table)


if __name__ == "__main__":
    main()
