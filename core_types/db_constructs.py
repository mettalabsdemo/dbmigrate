import sys
# db drivers
import MySQLdb
# project imports
from type_mappings.psql_mysql import ps_mysql_map
from type_mappings.psql_mysql.mysql_types import sql_types


class Database(object):

    def __init__(self, **kwargs):
        try:
            # self.sql_flavor (str)
            self.sql_flavor = kwargs["db_config"]["sql_flavor"]
            # self.host (str)
            self.host = kwargs["db_config"]["hostname"]
            # self.db (str)
            self.db = kwargs["db_config"]["database"]
            # self.user (str)
            self.user = kwargs["db_config"]["username"]
            # self.pwd (str)
            self.pwd = kwargs["db_config"]["password"]
            # self.credential_file (str)
            self.credential_file = kwargs["credentials"]
            # self.app_name (str)
            self.app_name = kwargs["app_name"]
            # self.app_root (str)
            self.app_root = kwargs["app_config"]
            # self.target (str) - Target DB that we want to compare this DB with
            self.target = kwargs["target"]
            # self.tables (list(Table)), self.export_tables (list(str))
            self.tables, self.export_tables = [], None
            # self.logger (logger to be used by this object for logging)
            self.logger = kwargs["logger"]
            self.DDL_statements = None
            self.ddl_file = None
            self.source_schema_file = self.app_root + \
                "/schema.sql"
        except KeyError, e:
            print e
            pass

    def set_tables_to_export(self, tables_to_export):
        """
        Sets the tables to be exported for both schema and data
        Args:
            tables_to_export (list): List of tables to be exported

        Returns:

        """
        print "Setting tables to export"
        self.export_tables = tables_to_export

    def add_table(self, table_name, target_sql_flavor):
        table = Table(table_name, target_sql_flavor, self.logger)
        self.tables.append(table)
        return table

    def get_tables(self):
        return self.tables

    def get_table(self, table_name):
        """
        Returns the table object for a table with given name
        Args:
            table_name (str): The name of the table to be returned

        Returns:
            (Table) - The table with given name if exists or else None
        """
        table = None
        for t in self.tables:
            if t.table_name == table_name:
                table = t
                break
        return table

    def field_datatype(self, table_name, field_name):
        """
        Returns the datatype of a field in a given table
        Args:
            table_name (str): The table to return the datatype from
            field_name (str): The name of the field

        Returns:
            (str) - The data type of the field, None if field doesnt exist
        """
        current_table = None
        for table in self.tables:
            if table.table_name == table_name:
                current_table = table
                break
        try:
            return current_table.field_data_type(field_name)
        except TypeError:
            pass


class Table(object):
    def __init__(self, table_name, columns=[]):
        """
        Returns a new instance of a table with given name and columns
        Args:
            table_name (str): Name of the table
            columns (list(Columns)): List of columns in this table

        Returns:
            (Table)
        """
        self.table_name = table_name
        self.columns = columns
        # self.logger = Logger("core_types.Table_logger", "./app.log")

    def add_column(self, column):
        """
        Adds a column to the given table
        Args:
            column (Column): The column to be added

        Returns:
            None
        """
        self.columns.append(column)

    def column(self, column_name):
        """
        Returns a column with a given name
        Args:
            column_name (str): Name of the column to be returned

        Returns:
            (Column) - Column with given name
        """
        return None

    def get_columns(self,):
        """

        Returns:
            (list(Columns)) - Returns all columns in this table sorted by the
            ordinal position of the column
        """
        return sorted(self.columns,
                      key=lambda column: column.ordinal_position)


class Column(object):
    def __init__(self, **kwargs):
        """
        Returns a new column with attributes defined in kwargs
        Args:
            **kwargs (keyword args): Args with column_name, datatype,
             length, ordinal_position, nullable, is_pkey, default

        Returns:
            (Column)
        """
        # self.logger = Logger("core_types.Column", "./app.log")
        try:
            self.column_name = kwargs["column_name"]
            self.data_type = kwargs["data_type"]
            self.character_maximum_length = kwargs["character_maximum_length"]
            self.ordinal_position = kwargs["ordinal_position"]
            self.is_nullable = kwargs["is_nullable"]
            self.is_pkey = kwargs["is_pkey"]
            self.column_default = kwargs["column_default"]
            self.auto_increment = ""
            self.mysql_type = None
            self.is_cast = False
            self.cast_type = None
        except KeyError, k:
            # self.logger.critical("{0} missing in column creation. Exiting".
            #                      format(k))
            print k

    def set_pkey(self, is_pkey):
        self.is_pkey = is_pkey

    def __repr__(self):
        return "Column Name: {column_name}; Datatype: {data_type}; " \
               "Character Length: {character_maximum_length}; " \
               "Is Nullable: {is_nullable}; Is PKey: {is_pkey}; " \
               "Default value: {column_default}; " \
               "Ordinal position: {ordinal_position}".format(
                column_name=self.column_name, data_type=self.data_type,
                character_maximum_length=self.character_maximum_length,
                is_nullable=self.is_nullable, is_pkey=self.is_pkey,
                column_default=self.column_default,
                ordinal_position=self.ordinal_position
                )


