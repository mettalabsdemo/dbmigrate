import sys
from concurrent.futures import ThreadPoolExecutor, as_completed, wait, ALL_COMPLETED
# db driver
import MySQLdb
import re
# app imports
from core_types.db_constructs import Database
from core_types.db_constructs import Table
from shell_utils import shell_util
from applogger.logger import Logger


class MySQLDB(Database):

    # SCHEMA_LOAD_CMD (str) - bash command to load schema into DB
    SCHEMA_LOAD_CMD = "shell_scripts/load_mysql_schema.sh {db} {schema} " \
                      "{host} {user} {pwd}"
    # BULK_LOAD_CMD (str) - MySQL command to bulk load a file into DB
    BULK_LOAD_CMD = r"LOAD DATA LOCAL INFILE '{fqfn}' INTO " \
                    r"TABLE {db}.{table}"

    # stored_proc_load (str) - Shell command to load a stored procedure
    # script into DB
    stored_proc_load = "mysql --host={host} " \
                       "--user={username} " \
                       "--password={password} " \
                       "{db} < " \
                       "{stored_proc_script}"

    # column_type_query (str) - query to return the datatype of a column
    column_type_query = 'SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS  ' \
                        'where table_name="{0}" and COLUMN_NAME = {1};'

    def __init__(self, **kwargs):
        kwargs["db_config"]["sql_flavor"] = "mysql"
        kwargs["db_config"]["database"] = kwargs["app_name"]
        self.logger = Logger("MySQL_LogHandler",kwargs["logroot"]+"/log")
        super(MySQLDB, self).__init__(
            db_config=kwargs["db_config"],
            credentials=kwargs["credentials"],
            app_name=kwargs["app_name"],
            app_config=kwargs["app_config"],
            logger=self.logger)
        self.thread_pool_executor = ThreadPoolExecutor(max_workers=5)
        self.bulk_load_jobs = []
        self.bulk_load_status = False

    def create_mysql_db(self, ddl_file_name):
        self.logger.info("Creating MySQLDB ")
        self.logger.info("Creating MySQLDB {0}".format(ddl_file_name))
        schema_load_command = MySQLDB.SCHEMA_LOAD_CMD.format(
            db=self.db,
            schema=ddl_file_name,
            host=self.host,
            user=self.user,
            pwd=self.pwd.replace(")","\\)")
        )
        self.logger.debug(schema_load_command)
        try:
            shell_util.execute_command(schema_load_command)
            self.logger.info("Schema file {0} loaded successfully into {1}".
                             format(ddl_file_name, self.host))
            return True
        except:
            self.logger.critical("Error loading schema. Will terminate")
            raise

    def connect_my_sql_server(self):
        conn = MySQLdb.connect(self.host, self.user, self.pwd, local_infile=1)
        return conn

    def connect_my_sql_server(self, db_name=None):
        if db_name:
            self.logger.debug(db_name)
            # conn (MySQLdb Connection)
            conn = MySQLdb.connect(self.host, self.user, self.pwd, db_name, local_infile=1)
            self.logger.debug(conn)
            return conn
        else:
            return MySQLdb.connect(self.host, self.user, self.pwd, local_infile=1)

    def bulk_load_thread_executor(self, caller_csv_writer):
        self.bulk_load_status = False
        self.bulk_load_jobs.append(self.thread_pool_executor.submit(self.bulk_load_file, caller_csv_writer))
        wait(self.bulk_load_jobs, timeout=None, return_when=ALL_COMPLETED)
        print "ALL COMPLETED"
        self.bulk_load_status = True

    def bulk_load_file(self, caller_csv_writer):
        file_name = caller_csv_writer.name
        self.logger.debug("BULK LOADING {0}".format(file_name))
        fqfn = "{0}/{1}.tsv".format(self.app_root, file_name)
        self.logger.debug(fqfn)
        print self.bulk_load_status
        try:
            conn = self.connect_my_sql_server()
            cursor = conn.cursor()
            self.logger.debug(self.BULK_LOAD_CMD.format(fqfn=fqfn,
                                            db=self.app_name,
                                            table=file_name))
            cursor.execute(self.BULK_LOAD_CMD.format(fqfn=fqfn,
                                                     db=self.app_name,
                                                     table=file_name))
            cursor.connection.commit()
            cursor.close()
            conn.close()
        except:
            print sys.exc_info()
            raise

    def bulk_load_file1(self, caller_csv_writer, table):
        file_name = caller_csv_writer
        print "BULK LOADING " + file_name
        fqfn = file_name
        try:
            conn = self.connect_my_sql_server()
            cursor = conn.cursor()
            print self.BULK_LOAD_CMD.format(fqfn=fqfn,
                                            db=self.app_name,
                                            table=file_name)
            cursor.execute(self.BULK_LOAD_CMD.format(fqfn=fqfn,
                                                     db=self.app_name,
                                                     table=table))
            cursor.connection.commit()
            cursor.close()
            conn.close()
        except:
            print sys.exc_info()
            raise

    def load_stored_proc(self, sp_script):
        """
        Loads a stored procedure from script file. Calls execute_command from
        shell util

        Arguments:
        sp_script(str) - Fully qualified file name of the procedure to load

        Returns:
        (int) status of the subprocess execution
        """
        sp_command = MySQLDB.stored_proc_load.format(
            host=self.host,
            username=self.user,
            password=self.pwd,
            db=self.app_name,
            stored_proc_script=sp_script
        )
        self.logger.debug("Loading stored procedures in {0} with command {1}".
                         format(sp_script, sp_command))
        return shell_util.execute_command(sp_command)

    def execute_stored_proc(self, **kwargs):
        """
        Executes a stored procedcure with its given arguments

        Arguments:
        kwargs (keyword arguments): containing the procedure name as str and
        procedure params as a tuple

        Returns:
        None
        """
        try:
            self.logger.info("Calling procecure {0} with {1}".format(
                             kwargs["procedure_name"],
                             kwargs["procedure_args"]))
            conn = self.connect_my_sql_server(db_name=self.app_name)
            cursor = conn.cursor()
            res = cursor.callproc(kwargs["procedure_name"],
                                  kwargs["procedure_args"])
            cursor.close()
            conn.close()
        except MySQLdb.Error, e:
            self.logger.critical("Error {0} executing procedure {1} with {2}".
                                 format(e, kwargs["procedure_name"],
                                        kwargs["procedure_args"]))
            pass

    def execute_select_query(self, table_name):
        query = "SELECT * FROM " + table_name + " LIMIT 1000"
        conn = self.connect_my_sql_server(db_name=self.app_name)
        cursor = conn.cursor()
        cursor.execute(query)
        res = cursor.fetchall()
        cursor.close()
        conn.close()
        return [r for r in res]

    def alter_column_name(self, **kwargs):
        """
        Alters the column name of the column given in kwargs["current_name"] with
        kwargs["new_name"]. Sets the column definition to kwargs["schema"]
        Args:
            **kwargs (keyword params) - keyword params containing the current name
            new name

        Returns:

        """
        current_field = kwargs["schema"]
        field_template = "`{current_name}` `{new_name}` {my_sql_type} " \
                         "{default_value} {nullable}" \
                         " {auto_increment} {primary_key}"
        field_ser = field_template.format(
                    current_name=kwargs["current_name"],
                    new_name=kwargs["new_name"],
                    my_sql_type=current_field["f_mysql_type"],
                    default_value=current_field["default_value"]
                    if current_field["primary_key"] != "PRIMARY KEY"
                    else "",
                    nullable=current_field["is_nullable"],
                    auto_increment=current_field["auto_increment_value"],
                    primary_key=current_field["primary_key"])
        alter_table_query = "ALTER TABLE `{db_name}`.`{table_name}` " \
                            "CHANGE {params}".format(
                                                    db_name=kwargs["dbname"],
                                                    table_name=kwargs
                                                        ["table_name"],
                                                    params=field_ser)
        self.logger.debug("Changing " + kwargs["current_name"] +
                          " to " + kwargs["new_name"] + "with query " +
                          alter_table_query)
        try:
            conn = self.connect_my_sql_server(db_name=kwargs["dbname"])
            cursor = conn.cursor()
            cursor.execute(alter_table_query)
            cursor.close()
            conn.close()
            return True
        except MySQLdb.Error, e:
            self.logger.critical(e)
            self.logger.critical("Error executing alter table. Column "
                                 "will be ignored in Diff")

    def drop_tables(self, tables=None):
        tables_to_delete = tables if tables else\
            ["table_diff_report"]
