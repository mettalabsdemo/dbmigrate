import os
from os.path import expanduser
from shutil import rmtree
import datetime
import sys
import time
import yaml
from docopt import docopt
# application package imports
from core_types.pgsqldb import PGSQLDB
from core_types.mysqldb import MySQLDB
from ddl_writers.ddl_parser import PSQLParser
from applogger.logger import Logger


# query_tables (list) list of tables that will be queried for report generation
APP_QUERY_TABLES = ["exclude_list", "table_diff_report", ]
# logger (None) Global logger that is set to None. Set to an instance of Logger
# in main once we have the path to the log file
LOGGER = None
# PGSQL_INSTANCE (PGSQLDB) object to handle Greenplum methods. Initialized in
# return_gp_instance
PGSQL_INSTANCE = None
# MYSQL_INSTANCE (MySQLDB) object to handle MySQL methods. Initialized in
# return_db_instances
MYSQL_INSTANCE = None


def create_db_instances(pgsql_params, mysql_params):
    """
    Instantiates PGSQLDB and MySQLDB objects.

    Args:
        pgsql_params (dict): Dictionary containing params for PGSQL connection
        mysql_params (dict): Dictionary containing params for MySQL connection

    Returns:

    """
    global PGSQL_INSTANCE, MYSQL_INSTANCE
    # MYSQL_INSTANCE (MySQLDB)
    if mysql_params["db_config"]:
        MYSQL_INSTANCE = MySQLDB(db_config=mysql_params["db_config"],
                                 credentials=mysql_params["credentials"],
                                 app_name=mysql_params["app_name"],
                                 app_config=mysql_params["app_config"],
                                 logroot=mysql_params["logroot"])
        LOGGER.debug("MySQL created")
    else:
        MYSQL_INSTANCE = None
    # PGSQL_INSTANCE (PGSQLDB)
    PGSQL_INSTANCE = PGSQLDB(db_config=pgsql_params["db_config"],
                             app_name=pgsql_params["app_name"],
                             app_config=pgsql_params["app_config"],
                             credentials=pgsql_params["credentials"],
                             logroot=pgsql_params["logroot"],
                             is_source=True,
                             export_only=True if MYSQL_INSTANCE else False,
                             target="mysql",
                             export_schema=pgsql_params["export_schema"])


def pgsql_init_schema_data_dump(export_tables):
    """
    Creates a dump of the pgsql schema by running the pgsql_dump command.

    Args:
        export_tables:

    Returns:
        (str): The exported schema file if export is successful,
        else None.
    """
    if export_tables:
        PGSQL_INSTANCE.set_tables_to_export(export_tables)
    LOGGER.debug("Begin PGSQL export")
    # Method that should change
    PGSQL_INSTANCE.export_pgsql_schema()
    LOGGER.debug("End PGSQL export")
    # exported_schema_file (str)
    PGSQL_INSTANCE.convert_psql_mysql()
    return PGSQL_INSTANCE.convert_psql_mysql()


def run_table_diff(**kwargs):
    MYSQL_INSTANCE.load_stored_proc(kwargs["sp_file"])
    MYSQL_INSTANCE.execute_stored_proc(procedure_name="missing_columns",
                                       procedure_args=(MYSQL_INSTANCE.db,
                                                       kwargs["target"],
                                                       kwargs["table"]))
    return MYSQL_INSTANCE.execute_select_query("missing_columns")


def load_schema_run_table_diff(is_diff=True, **kwargs):
    """
    Creates an instance of MySQLDB and calls the method to load the schema file.
    Once the schema is loaded, makes a call to the stored procedure to run the
    diff between two table schemas. If diffs exist, this function calls the
    report generator else returns the MySQLDB object.

    Args:
        is_diff (bool) - Indicates if diff be executed on this run
        kwargs (keyword Args) - contains info needed to connect to mysql,
        configure the MySQL connection object, and the exported schema file

    Returns:
        list(((str,str),(str,str))) -A list of 2 tuples containing the imported DB
        column name and target DB column names that are missing.
    """
    LOGGER.debug("Initializing schema import into MySQL")
    LOGGER.debug("Initializing schema import into MySQL")
    print "Before creating schema"
    MYSQL_INSTANCE.create_mysql_db(kwargs["exported_file"])
    print "after creating schema"
    if is_diff:
        return run_table_diff(kwargs)


def create_missing_columns_report(**kwargs):
    """
    Creates an YAML file that stores the application info
    along with the missing columns
    Args:
        kwargs (keyword args): Keyword arguments containing
            missing_columns (list(str,str)) - A list of two tuples containing the
            db and the missing column for this table
            tables (list(str)) - list of tables to be exported
            columns (list(str)) - list of columns to be excluded in compare
            artifact_root (str) - the folder to write the report into
            app_name (str) - the name of the application creating the report.
            This is also the name of the DB data is being imported into in MySQL
            pgsqldb (str) - The Greenplum DB data is being copied from
            pgsql_schema (str) - The schema in the Greenplum DB
            mysqldb (str) - MySQL DB compared to

    Returns:
        missing_columns_report (str) - file name of the generated report file
    """
    try:
        LOGGER.debug(kwargs["missing_columns"])
        missing_columns_report = kwargs["artifact_root"] + "/missing_column_report.yaml"
        yaml.safe_dump({"tables": kwargs["tables"],
                        "columns": kwargs["columns"],
                        "missing_columns": kwargs["missing_columns"],
                        "app_name": kwargs["app_name"],
                        "mapping": [{missing_column[1]:
                                         [PGSQL_INSTANCE.field_datatype(
                                             kwargs["tables"][0],
                                             missing_column[1]),
                                          None, ]
                                     for missing_column in
                                     kwargs["missing_columns"]
                                     if missing_column[0] ==
                                     kwargs["app_name"]}],
                        "pgsqldb": kwargs["pgsqldb"],
                        "pgsql_schema": kwargs["pgsql_schema"],
                        "mysqldb": kwargs["mysqldb"], },
                       open(missing_columns_report, "w"))
        return missing_columns_report
    except IOError:
        LOGGER.critical("Could not write to {0}".format(
            kwargs["artifact_root"]))


def load_application_from_report(missing_columns_report):
    """
    Loads the application to restore from the missing fields Yaml report.

    Args:
        missing_columns_report: Fully qualified config yaml file name

    Returns:
        pgsql_config (dict) - dictionary contaning pgsql_config information.
        mysql_config (dict) - dictionary contaning mysql_config information.

    """
    try:
        # config (str) - loads the configuration from YAML
        config = None
        with open(missing_columns_report) as resume_config:
            config = yaml.safe_load(resume_config)
        return config["tables"], config["mapping"], config["mysqldb"],\
            config["columns"]
    except IOError:
        LOGGER.critical(sys.exc_info())
        print "Error loading config file"
    except KeyError, key:
        print "Config file does not contain required params"
        print key


def copy_data(tables_to_copy):
    """
    Starts the threaded sequence to copy data from Greenplum to MySQL.
    Monitors the state of all threads and returns once all threads
    are done.

    Args:
        tables_to_copy (list(str)): A list of tables to be copied

    Returns:
        True after all copy is finished.
    """
    LOGGER.debug("Initializing copy of {0} from Greenplum to MySQL".
                 format(tables_to_copy))
    PGSQL_INSTANCE.set_tables_to_export(tables_to_copy)
    PGSQL_INSTANCE.copy_to_csv_init(MYSQL_INSTANCE.bulk_load_thread_executor)
    pending_jobs = True
    while pending_jobs:
        if PGSQL_INSTANCE.pg_dump_status and MYSQL_INSTANCE.bulk_load_status:
            pending_jobs = False
        else:
            time.sleep(2)
    return True


def create_artifact_directories(app_dir_name):
    """
    Creates the directories to which generated files will be written to.
    The directory structure is ~/dbcopy/appname/...
    Args:
        app_dir_name (str): The name of the directory to be created

    Returns:

    """
    try:
        os.makedirs(app_dir_name)
    except OSError:
        # log directory exists
        print sys.exc_info()


def initialize_application_env(artifact_root, db_name, schema, target_db=None):
    """
    Initialize the directories that the application will write data to.
    These will be created under the application root. Application root has the
    pattern user_home/dbcopy/application_name. Application
    name is generated by combining the Greenplum DB Name followed by constant
    string SCH followed by Greenplum schema name followed by the current time.
    The time format is Year Month Day Hour Minute. This name is also used as the
    database name when the copy is created in MySQL. Greenplum export files are
    written under application root/pgsql. MySQL files are written under
    application root/mysql. The application logs are written in application
    root/logs.

    Args:
        artifact_root (str): The parent folder to create all assets in
        db_name (str): Greenplum DB name
        schema (str): Greenplum schema
        target_db (str): The name of the MySQLDB if given in the configuration

    Returns:
        app_name (str): The generated application name
        pgsql_root (str): Greenplum root under which Greenplum files are stored
        mysql_root (str): MySQL root under which MySQL files are stored
        log_root (str): Logging directory for storing log files

    Exceptions:
    None will returned if there are errors in directory creation.

    """

    # app_name (str)
    if target_db:
        app_name = target_db
    else:
        app_name = "{db_name}_SCH_{schema}_{dt}".format(db_name=db_name,
                                                        schema=schema,
                                                        dt=datetime.datetime.
                                                        utcnow().
                                                        strftime("%d%H%m"))
    # quick  fix to handle the 64 character mysql bug
    app_name = app_name[:63]
    # app_root (str) - the root folder where all files for this application
    # will be stored


    app_root = artifact_root + "/{app_name}".format(app_name=app_name)
    # pgsql_root (str)
    pgsql_root = app_root + "/pgsql"
    # mysql_root (str)
    mysql_root = app_root + "/mysql"
    # log_root (str)
    log_root = app_root + "/logs"
    # create application directories
    create_artifact_directories(pgsql_root)
    create_artifact_directories(mysql_root)
    create_artifact_directories(log_root)
    return app_name, pgsql_root, mysql_root, log_root


def check_and_create_pgpass(artifact_root, host, database, user, pwd, ):
    """
    Check if credentials are present in the conf directory for PGSQL.
    If not create a pgpass file.

    Args:
        artifact_root (str): The root directory to create all assets into
        host (str): host name of the Greenplum server
        database (str): the database to connect to
        user (str): DB server username
        pwd (str): DB server password

    Returns:

    pgpass_file (str) - The path to the file created

    Exceptions:

    Raises IOError if file cannot created
    """
    # pgpass_file (str)
    pgpass_file = artifact_root + "/conf/pgpass"
    try:
        if not os.path.isfile(pgpass_file):
            create_artifact_directories(artifact_root + "/conf")
            # create a temp password file. delete on exit
            with open(pgpass_file, "w") as pass_file:
                pass_file.write("{host}:*:{db}:{user}:{pwd}".format(
                    host=host, db=database, user=user, pwd=pwd
                ))
            LOGGER.info("Greenplum credentials created in {0}".
                        format(pgpass_file))
        else:
            # log pgpass exisits.
            LOGGER.info("Greenplum credentials exists in {0}".
                        format(pgpass_file))
        os.chmod(pgpass_file, 0600)
        return pgpass_file
    except IOError:
        print "Cannot stat pgpass. Check permissions"
        raise IOError


def parse_config_file(configs):
    """
    Parses the config yaml file for this application and returns two
    dictionaries, one for pgsql and one for mysql. Both dictionaries have
    hostname, database, username, and password keys.Checks for IOError for
    file and Keyerror to ensure configs are present. In case both Exceptions,
    returns None.

    Args:
        configs (str): Fully qualified config yaml file name

    Returns:
        pgsql_config (dict):  dictionary contaning pgsql_config information.
        mysql_config (dict):  dictionary contaning mysql_config information.

    """
    # pgsql_config (dict)
    # mysql_config (dict)
    # config (dict)
    # output_dir (str)
    config, pgsql_config, mysql_config, output_dir = None, None, None, None
    try:
        with open(configs) as pgmysql_config:
            config = yaml.safe_load(pgmysql_config)
        print config
    except IOError:
        print "Error loading config file"
    except Exception:
        print sys.exc_info()
    try:
        mysql_config = config["mysql"]
    except KeyError:
        mysql_config = None
    try:
        pgsql_config = config["pgsql"]
    except KeyError:
        print "Config file does not contain required params"
        return None
    except Exception:
        print sys.exc_info()
    try:
        output_dir = config["output_directory"]
    except KeyError, key:
        print "Output directory not found in conf. Will use home/data"
        output_dir = "/home/data"
    try:
        export_schema = config["export_schema"]
    except KeyError:
        export_schema = None
    try:
        target_db = config["target_db"]
    except KeyError:
        target_db = None
    except Exception:
        print sys.exc_info()
    return pgsql_config, mysql_config, output_dir, export_schema, target_db


def configure_application(application_args):
    """
    Parse the command line Args and set the application configuration.
    Configuration includes if this process is running in diff mode and also
    if it is a resume of a previously stopped process. Checks for matching
    params for each of these options and returns a dictionary with application
    configuration options.

    Args:
        application_args (dict): contains the keyword Args parsed by DocOpt

    Returns:
    app_config(dict): returns a dictionary with application configuration
    options.
    """
    # app_config (dict). The mode key indicates if the app is in copy, resume,
    # or diff mode. The mode type is a key that contains the values for
    # executing in that mode. For example, if mode is "copy", there will be a
    # entry in the dictionary with key "copy"
    app_config = {"mode": None, "config": application_args["--config"], }
    # is_resume (boolean)
    is_resume = True if application_args["--resume"] else False
    # is_diff (boolean)
    is_diff = True if application_args["--diff"] else False
    # application cannot be both diff and resume. Raise and exit
    if not (is_resume and is_diff):
        # for diff mode check if table and DB are present
        if is_diff:
            if application_args["--table"] and application_args["TARGET_DB"]:
                app_config["diff"] = {"table": application_args["--table"],
                                      "targetDB": application_args["TARGET_DB"],
                                      "spfile": application_args["--file"],
                                      "columns": application_args["COLUMNS"],
                                     }
                return app_config
            else:
                print "Cannot run in diff mode without table and target DB"
        elif is_resume:
            app_config["resume"] = {"app_name": application_args["--app"]}
            return app_config
        else:
            app_config["copy"] = {}
            return app_config
    else:
        print "Application cannot be run in diff and resume mode at the same"\
              "time. Please fix the params."


def do_copy():
    """

    Args:
    Returns:

    """
    LOGGER.debug("Initalizing copy")
    exported_file = pgsql_init_schema_data_dump(export_tables=None,)
    if MYSQL_INSTANCE:
        load_schema_run_table_diff(is_diff=False, exported_file=exported_file)
    copy_data(tables_to_copy=None)


def do_diff(diff_config, app_name, log_root, artifact_root, pgsql_config):
    """

    Args:
        diff_config (dict): Dict containing info to run the diff
        app_name (str): Application name
        log_root (str): The log folder to write the logs
        artifact_root (str): The base folder for this application
        pgsql_config (str): Greenplum connection object

    Returns:

    """
    # export_tables (str)
    export_tables = diff_config["table"]
    # stored_proc_file (str)
    stored_proc_file = diff_config["spfile"]
    # target_db (str)
    target_db = diff_config["targetDB"]
    # columns_to_ignore (str)
    columns_to_ignore = diff_config["columns"] if diff_config["columns"] else ""
    if stored_proc_file and not os.path.isfile(stored_proc_file):
        LOGGER.critical("Error reading stored procedure file at {0}"
                        .format(stored_proc_file))
        sys.exit(1)
    LOGGER.info("Starting {0} in diff mode ".format(app_name))
    # PGSQL_INSTANCE (PGSQLDB), exported_file (str)
    # export_tables (list(str))
    export_tables = [export_tables, ]
    exported_file = pgsql_init_schema_data_dump(
        export_tables=export_tables,)
    if exported_file:
        # mysql_diff_artifacts (list(tuple(str)))
        missing_columns = load_schema_run_table_diff(
            exported_file=exported_file,
            logroot=log_root,
            sp_file=stored_proc_file,
            target=target_db,
            table=export_tables[0])
        if len(missing_columns):
            missing_columns_report = create_missing_columns_report(
                missing_columns=missing_columns,
                tables=export_tables,
                columns=columns_to_ignore,
                artifact_root=artifact_root + "/" + app_name,
                app_name=app_name,
                pgsqldb=pgsql_config["database"],
                pgsql_schema=pgsql_config["schema"],
                mysqldb=target_db)
            LOGGER.critical("Schemas are different between Greenplum"
                            " and MySQL. Edit the mapping file at {0}"
                            " and start application in resume mode".
                            format(missing_columns_report))
        else:
            copy_data_exec_stored_proc(tables=export_tables[0],
                                       columns_to_ignore=columns_to_ignore,
                                       target_db=target_db,
                                       resource_root=[artifact_root,
                                                      app_name, ])

    else:
        LOGGER.critical("{0} failed in exporting schema".format(app_name))
        sys.exit(1)


def cleanup_files(artifact_root):
    """
    Delete all the files except logs created by this application.

    Args:
        artifact_root (str) - The artifact root of this application

    Returns:
        (bool) - True if delete is successful
    """
    try:
        rmtree(artifact_root + "/pgsql")
        rmtree(artifact_root + "/mysql")
        LOGGER.debug("Successfully removed files created")
        return True
    except IOError:
        LOGGER.critical("Cannot stat or delete generated files. Kindly"
                        " delete files at {0}".format(artifact_root))


def copy_data_exec_stored_proc(tables=None, columns_to_ignore=None,
                               resource_root=None, target_db=None):
    """
    Calls the functions to copy the data from GP to MySQL,
    execute the stored proc on MySQL, generate report,
    drop the procedure, and delete all the data files created
    in this run.
    Args:
        tables (list): tables to be copied
        columns_to_ignore (list): columns to be ignore while running diff
        resource_root (list): the root folder to delete the artifacts from
        target_db (str): The target database to run diff against

    Returns:

    """
    if copy_data(tables_to_copy=tables):
        MYSQL_INSTANCE.execute_stored_proc(
            procedure_name="table_diff",
            procedure_args=(resource_root[1],
                            target_db,
                            tables[0],
                            ",".join(columns_to_ignore)))
        cleanup_files("/".join(resource_root))


def main(cmd_line_args):
    """
    Main method
    Args:
        cmd_line_args (dict): Dictionary generated by docopt containing
        command line args

    Returns:

    """
    # app_config (dict)
    app_config = configure_application(cmd_line_args)
    try:
        # check if we have the credentials and can create a credentials file.
        # pgsql_config (dict), mysql_config (dict)
        pgsql_config, mysql_config, output_dir, export_schema, target_db = \
                                 parse_config_file(app_config["config"])\
                                 if app_config["config"]\
                                 else\
                                 parse_config_file(
                                     expanduser("~") +
                                     "/dbcopy/conf/pg2mysql.yml")
        # artifact_root (str)
        artifact_root = (output_dir if output_dir else expanduser("~"))\
            + "/dbcopy"
        # set up the application config
        if "resume" in app_config:
            app_name = app_config["resume"]["app_name"]
            pgsql_app_config = artifact_root + "/" + app_name + "/pgsql"
            mysql_app_config = artifact_root + "/" + app_name + "/mysql"
            log_root = artifact_root + "/" + app_name + "/logs"
        else:
            print target_db
            app_name, pgsql_app_config, mysql_app_config, log_root = \
                initialize_application_env(artifact_root,
                                           pgsql_config["database"],
                                           pgsql_config["schema"],
                                           target_db=target_db)
        # initialize logger
        print app_name
        global LOGGER

        print log_root + "/log"
        LOGGER = Logger("pg2mysqlMainController", log_root + "/log")
        LOGGER.info("{0} initialized.".format(app_name))
        LOGGER.info("Greenplum artifacts produced will be written in {0}".
                    format(pgsql_app_config))
        LOGGER.info("MySQL artifacts produced will be written in {0}".
                    format(mysql_app_config))
        LOGGER.info("Logs will be written in {0}".
                    format(log_root))
        # pgsql_credentials_file (str)
        pgsql_credentials_file = check_and_create_pgpass(artifact_root,
                                                         pgsql_config["hostname"],
                                                         pgsql_config["database"],
                                                         pgsql_config["username"],
                                                         pgsql_config["password"])

        create_db_instances(pgsql_params={"db_config": pgsql_config,
                                          "app_name": app_name,
                                          "app_config": pgsql_app_config,
                                          "credentials":
                                              pgsql_credentials_file,
                                          "logroot": log_root,
                                          "export_schema": export_schema,
                                          "target": target_db},
                            mysql_params={"db_config": mysql_config,
                                          "app_name": app_name,
                                          "app_config": pgsql_app_config,
                                          "credentials":
                                              pgsql_credentials_file,
                                          "logroot": log_root,})
    except TypeError, t:
        LOGGER.critical(t)
        sys.exc_info()
        sys.exit(1)
    except IOError:
        sys.exc_info()
        sys.exit(1)
    except KeyError, k:
        print k
        print "Error parsing configuration. See below"
        sys.exc_info()
        sys.exit(1)
    except Exception:
        sys.exc_info()
    try:
        # copy_config (dict)
        copy_config = app_config["copy"]
        LOGGER.debug(copy_config)
        do_copy()
        # copy mode logic to be implemented here
    except KeyError:
        pass
    except TypeError, t:
        # parse_Args_... returned None. Application should exit.
        LOGGER.critical("{0} failed to parse Args".format(app_name))
        LOGGER.critical(t)
        print sys.exc_info()
        sys.exit(1)
    try:
        # diff_config (dict)
        diff_config = app_config["diff"]
        do_diff(diff_config, app_name, log_root, artifact_root, pgsql_config)
    except KeyError, k:
        print k
    try:
        # resume_config (dict)
        LOGGER.debug(app_config)
        resume_config = app_config["resume"]
        missing_columns_report = artifact_root + "/" +\
            resume_config["app_name"] + "/" + "missing_column_report.yaml"
        tables, mappings, target_db, columns_to_ignore = \
            load_application_from_report(
                missing_columns_report)
        for mapping in mappings:
            for current_mapping_key in mapping:
                # append_to_ignore (bool) - status if a column should be ignored
                append_to_ignore = False
                LOGGER.debug(mapping[current_mapping_key])
                if mapping[current_mapping_key][1]:
                    if not MYSQL_INSTANCE.alter_column_name(
                            current_name=current_mapping_key,
                            new_name=mapping[current_mapping_key][1],
                            schema=mapping[current_mapping_key][0],
                            table_name=tables[0],
                            dbname=resume_config["app_name"]):
                        append_to_ignore = True
                else:
                    append_to_ignore = True
                if append_to_ignore:
                    columns_to_ignore.append(mapping[current_mapping_key])
        copy_data_exec_stored_proc(tables=tables,
                                   columns_to_ignore=columns_to_ignore,
                                   resource_root=[artifact_root, app_name, ],
                                   target_db=target_db)
    except KeyError, k:
        print k
        # mysql_schema (str)
        # create stored procedure
        # MYSQL_INSTANCE.load_stored_proc(stored_proc_file)
        #

if __name__ == '__main__':
    arguments = docopt(__doc__, version='1.0.0rc2')
    main(arguments)
